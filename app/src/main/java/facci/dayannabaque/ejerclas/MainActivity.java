package facci.dayannabaque.ejerclas;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import java.util.List;

import facci.dayannabaque.ejerclas.adaptador.ChistesAdapter;
import facci.dayannabaque.ejerclas.modelo.Chiste;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getChistes();
    }
 
    public void getChistes(){
        ChistesAdapter chistesAdapter =new ChistesAdapter();
        Call<Chiste> call = chistesAdapter.getChistes();
        call.enqueue(new Callback<Chiste>() {
            @Override
            public void onResponse(Call<Chiste> call, Response<Chiste> response) {
                Chiste chiste = response.body();
             /*   for (Chiste chiste : Listachiste) {
*/
                Log.i("Aviso", chiste.getChiste());

            }


            @Override
            public void onFailure(Call<Chiste> call, Throwable t) {

            }
        });
    }

}